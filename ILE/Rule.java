package ILE;

import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class Rule {
    
    public static final boolean YES=true;
    public static final boolean NO=false;

    Vector<Double> coeff;
    double outerRadius;
    double innerRadius;
    
    public Rule()
    {
        this.coeff =new Vector<Double>();
    }

    public Rule(Vector<Double> _coeff, double _outerRadius, double _innerRadius)
    {
        this.coeff = _coeff;
        this.outerRadius = _outerRadius;
        this.innerRadius = _innerRadius;
    }
    
    public Rule(Centroid _cent, double _outerRadius, double _innerRadius)
    {
        this.coeff = _cent.attributes;
        this.outerRadius = _outerRadius;
        this.innerRadius = _innerRadius;
    }
    
    public Rule(Rule r){
        this.coeff = r.coeff;
        this.outerRadius = r.outerRadius;
        this.innerRadius = r.innerRadius;
    }
    
    public boolean isC(Data d)
    {
        int numAttr=d.getAttr().size();
        int numCoeff=coeff.size();
        // if the number of attr in the data is diff from the number of coefficients in this rule
        // must not satisfy the rule
        if(numAttr!=numCoeff)
        {
            return NO;
        }

        //If the two numbers are equal, check which side of the hyperplane does the data belongs to
        double val=0.0;
        // (X1-C1)^2 + (X2-C2)^2 + ... + (Xn-Cn)^2 =?= R^2
        for(int i=0;i!=numAttr;i++)
        {
            val+=Math.pow(d.get(i)-coeff.get(i),2);
        }
        double val1 = val - Math.pow(outerRadius,2);
        double val2 = val - Math.pow(innerRadius,2);

        if(val1<=0 && val2>=0)//if within the range of (inner_radius,outer_radius), return YES
            return YES;
        return NO;//outside of the range, return NO.

    }
}
