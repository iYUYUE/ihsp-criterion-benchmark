package ILE;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class Trainer {

    DataPool dp;    //training data
    DataPool vdp; 
    
    //DataPool nearDataPool;
    int numCSample;
    int dpFinalNum = 0;
    double extendRate = 1.1;
    public static final int MAX_NUM_RULE = 1;
    public static final int MAX_NUM_ROUND = 10;
    GUIClass gui;
    boolean enableGUI = false;
    int level;

    /**
     *
     * <summary>Default constructor</summary>
     */
    public Trainer() {
    }

    /**
     *
     * <summary>Constructor initialized with a DataPool</summary>
     * <param name="_dp">DataPool</param>
     */
    public Trainer(DataPool _dp, DataPool _vdp, GUIClass gui, boolean enableGui, int lv) {
        dp = new DataPool(_dp);
        this.gui = gui;
        vdp=new DataPool(_vdp);
        this.enableGUI = enableGui;
        this.level = lv;
    }

    /**
     *
     * <summary>Constructor initialized with a DataPool</summary>
     * <param name="_dp">DataPool</param>
     */
    public Trainer(DataPool _dp, DataPool _vdp, GUIClass gui, boolean enableGui, double ext, int lv) {
        dp = new DataPool(_dp);
        this.gui = gui;
        vdp=new DataPool(_vdp);
        this.enableGUI = enableGui;
        this.extendRate = ext;
        this.level = lv;
    }
    
    /**
     * Train expG, solution to one Class, of class C.
     */
    public ExpertGroup train(double goal, int C)
    {
        /*variable preparation*/
        int numLearntSample=0;
        int numData=dp.getNumData();
        numCSample=0;

        /**Find the total number of data in class C*/
        for(int i=0;i!=numData;i++)
        {
            if(dp.getData(i).getDClass()==C){
                ++numCSample;
            }
        }
        if(this.enableGUI){
            // display gui
            gui.repaint();
        }
        /** Train the solution to one Class of data
            each rule in the exp is trained by calling trainSingleExpert() method
            correctly learned data will be removed from the remaining dataset
            training continues until all the data in that class have been learned
         */
        ExpertGroup expG=new ExpertGroup();
        while(numLearntSample<numCSample)
        {
            if(level>1)
                System.out.printf("Start Training New Expert.\n"
                        + "Learnt Sample: %d\tTotal Sample: %d\n\n", 
                        numLearntSample, numCSample);
            
            int numRemainingData=dp.getNumData();
            
            /*train a new expert*/
            Expert exp=trainSingleExpert(goal,C);
            
            // if the expert can only learn 1 pattern, catch the expert
            int numLearnt = 0;
            Data dVolatile = new Data();
            for(int i=0;i!=numRemainingData;i++){
                Data d=dp.getData(i);
                if(exp.judge(d)==true && d.getDClass()==C)
                {
                    numLearnt++;
                    dVolatile = d;
                }
            }
            if(numLearnt == 1){
                Rule newRule = new Rule(new Centroid(dVolatile.attributes), getLongestDistance(dVolatile), 0.0);
                exp = new Expert(C, newRule);
            }
            
            expG.addExpert(exp);
            //System.out.println("Rule number = "+exp.rules.size());
            
            /*Remove the newly learned data from the remaining dataset*/
            
            for(int i=0;i!=numRemainingData;i++){
                Data d=dp.getData(i);
                if(exp.judge(d)==true && d.getDClass()==C)
                {
                    dp.deleteData(i);
                    --numRemainingData;
                    ++numLearntSample;
                    --i;
                }
            }
            if (this.enableGUI) {
                gui.datapool = dp;
                gui.cent = new Centroid(exp.rules.get(exp.rules.size() - 1).coeff);
                gui.innerR = exp.rules.get(exp.rules.size() - 1).innerRadius;
                gui.outerR = exp.rules.get(exp.rules.size() - 1).outerRadius;
                gui.repaint();
                gui.cent.PrintCentroid();
            }
//            System.out.println("innerRadius = "+exp.rules.get(exp.rules.size() - 1).innerRadius+"\nouterRadius = "+exp.rules.get(exp.rules.size() - 1).outerRadius);
//            System.out.println("Remaining = "+ getSameC(C).getNumData());
        }
        return expG;
    }

    public Expert trainSingleExpert(double goal, int C){
        /**Variables preparation.*/
        DataPool targetCluster = this.getTargetCluster(dp,C);
        if(level>1)
            System.out.println("Near Size: " + targetCluster.getNumData());
        
        int numAttr=targetCluster.getNumAttr();
        double fitness=0.0;
        
        if(targetCluster.getNumData()<3)
        {
            //new Centroid(dpNear.getData(0).attributes).PrintCentroid();
            //Random rnd = new Random();
            // The outer radius should be as big as possible
            Rule newRule = new Rule(new Centroid(targetCluster.getData(0).attributes), getLongestDistance(targetCluster.getData(0)), 0.0);
            //Rule newRule = new Rule(new Centroid(dpNear.getData(0).attributes), 1+rnd.nextDouble(), 0.0);
            Expert exp = new Expert();
            exp.C = C;
            exp.addRule(newRule);
            return exp;
        }
        
        double lb=targetCluster.findLeftBound(0);
        double rb=targetCluster.findRightBound(0);
        //detect the range of the attributes. This is used when initialization, as the banduaries of the attri value.
        for(int i=0;i!=numAttr;i++)
        {
            double tlb=targetCluster.findLeftBound(i);
            double trb=targetCluster.findRightBound(i);
            if(lb>tlb)
                lb=tlb;
            if(rb<trb)
                rb=trb;
        }
                
        EEEncoder enc=new EEEncoder(1,numAttr,C,this.getCentroid(targetCluster),dp,lb,rb); //ONLY 1 rule in one chromsome!!!!
        EEDecoder dec=new EEDecoder(dp,1);
        EEDecoder vdec=new EEDecoder(vdp,1);
        
        Expert exp=dec.initializeExpert();
        exp.C=C;    // Set the class type of this training expert
        
        /** Train the expert. GASolver will be called to train the rules in that expert.
           The adding of new rule into exp will continue until either:
           1. Fitness of the newly trained rule reaches the goal. ! fitness of the rule not the expert.!
          or 2. size of the rules in exp exceeds the limit.
        */
        GASolver ga=new GASolver(1.0,0.02,300,150,2,dec,enc); //the matingPoolSize is half the size of population!!!!!
        int round = 0;
        while(fitness<goal && round<MAX_NUM_ROUND)//exp.rules.size()<=MAX_NUM_RULE)
        {
            /*Calling GA to train the best chrom*/
            ga.train(30,goal);
            Chromosome bestChrom=ga.chroms.get(ga.bestChromIndex);
            if(vdp.getNumData()!=0){
                // Exist validation group, Then do the validation
                fitness=vdec.fitness(bestChrom);
            }else{
                // No validation
                fitness=dec.fitness(bestChrom);
            }

            /*Construct a new rule based on the best trained Chrom.*/
            // construct the coefficients of the rule
            Vector<Double> temp_coeff=new Vector<Double>(numAttr);
            for(int i=0;i!=numAttr;i++){
                double value=bestChrom.code.get(1+i);
                temp_coeff.add(value);
            }
            // outer and inner radius
            double temp_outerRadius = bestChrom.code.get(1 + numAttr);
            double temp_innerRadius = bestChrom.code.get(2 + numAttr);

            /*add the new rule to the training exp*/
            Rule newRule=new Rule(temp_coeff, temp_outerRadius, temp_innerRadius);
            exp.addRule(newRule);
            round++;
        }
        return exp;
    }
    
    /**
     *
     * <summary>get the near data</summary>
     * <param name="dpOrigin">original DataPool</param>
     * <param name="distance">data</param>
     * <return>DataPool full of patterns near from each other</return>
     */
    public DataPool getTargetCluster(DataPool dpOrigin, int C) {
        double distance = 0.0;
        DataPool dpFinal = new DataPool();
        //Firstly copy a temp
        DataPool dpTemporary = new DataPool(dpOrigin);
        //Get the data of the same class
        DataPool dpSameC = getSameC(C);

        //Get a reasonable distance value
        Random rnd = new Random();
        Data dSelected = dpSameC.getData(rnd.nextInt(dpSameC.getNumData()));
        distance = this.generateDistance(dpSameC, dSelected);
        if(level>1)
            System.out.println("Distance: " + distance);
        
        dpFinal.addData(dSelected);
        
        //Iteratively get the nearest several particles.
        while (dpFinalNum != dpFinal.getNumData()) {
            dpFinalNum = dpFinal.getNumData();
            for (int i = 0; i < dpFinal.getNumData(); i++) {
                setTargetCluster_Recursive(dpSameC, dpFinal, dpFinal.getData(i), distance);
            }
            //System.out.println("size = "+ dpFinalNum);
        }

        //Delete all the clustered patterns in the same class
        dpTemporary.deleteData(dpFinal);

        return dpFinal;
    }

    /**
     *
     * <summary>recursively get the nearest data</summary>
     * <param name="dptemp">temporary DataPool</param>
     * <param name="particle">specific particle</param>
     * <return>DataPool full of patterns near from each other</return>
     */
    public DataPool getSameC(int C) {
        int totalParticleNum = dp.getNumData();
        DataPool dpSameC = new DataPool(dp.getNumAttr(), 1);
        for (int i = 0; i < totalParticleNum; i++) {
            if (dp.getData(i).C == C) {
                dpSameC.addData(dp.getData(i));
            }
        }
        return dpSameC;
    }

    /**
     *
     * <summary>recursively get the nearest data</summary>
     * <param name="dpSameC">DataPool with data in the same class</param>
     * <param name="dpFinal">Final DataPool</param>
     * <param name="particle">particle info</param>
     * <param name="distance">distance that is accepted</param>
     */
    public void setTargetCluster_Recursive(DataPool dpSameC, DataPool dpFinal, Data particle, double distance) {
        double tempNumber = 0.0;
        Data newParticle = new Data();

        for (int i = 0; i < dpSameC.getNumData(); i++) {
            for (int j = 0; j < particle.getNumAttr(); j++) {
                newParticle = dpSameC.getData(i);
                tempNumber += Math.pow(Math.abs(newParticle.getAttr().get(j) - particle.getAttr().get(j)), 2);
            }
            /**
             * * Checkpoint **
             */
            //System.out.println(newParticle.attributes.toString()+"\t"+particle.attributes.toString());
            //Get the direct distance below
            tempNumber = Math.pow(tempNumber, 0.5);
            if (tempNumber <= distance) // Near particle found
            {
                //Add the new particle
                dpFinal.addData(newParticle);
                //Delete the new particle from the dpSameC
                dpSameC.deleteData(newParticle);
            }
        }
    }

    /**
     *
     * <summary>automatically generate a reasonable distance</summary>
     * <param name="distance">default distance</param>
     * <param name="dpSameC">DataPool with patterns in the same class</param>
     * <return>a distance that can help find other patterns in this class</return>
     */
    private double generateDistance(DataPool dpSameC, Data dSelected) {
        double newDistance = 0.0;
        Boolean del = dpSameC.deleteData(dSelected);
        
        if (dpSameC.getNumData() >= 3) {
            double newDistanceRaw = 0.0;
            for (int i = 0; i < dpSameC.getData(0).getNumAttr(); i++) {
                newDistanceRaw += Math.abs(dSelected.attributes.get(i) - dpSameC.getData(1).attributes.get(i));
                        //- dpSameC.getData(1).attributes.get(i)), 2);
            }
            double minDistanceRaw = newDistanceRaw;
            int minIndex = 0;
            // Find and replace the minimum distance
            for (int j = 2; j < dpSameC.getNumData(); j++) {//dpSameC.getNumData(); j++) {
                newDistanceRaw=0.0;
                for (int i = 0; i < dSelected.getNumAttr(); i++) {
                    newDistanceRaw += Math.abs(dSelected.attributes.get(i) - dpSameC.getData(j).attributes.get(i));
                            //- dpSameC.getData(j).attributes.get(i)), 2);
                }
                minDistanceRaw = getMin(minDistanceRaw, newDistanceRaw);
                minIndex = minDistanceRaw == newDistanceRaw ? j : minIndex;
            }
            for (int i = 0; i < dpSameC.getData(minIndex).getNumAttr(); i++) {
                newDistance += Math.pow(Math.abs(dSelected.attributes.get(i) - dpSameC.getData(minIndex).attributes.get(i)),2);
            }
            double minDistance = Math.pow(newDistance, 0.5);
            return extendRate * minDistance;
        }
        return getLongestDistance(dSelected);
    }
    
    /**
     *
     * <summary>get a maximum effective distance that will not affect other classes</summary>
     * <param name="d">selected pattern</param>
     * <return>a maximum distance that will not affect other classes</return>
     */
    private double getLongestDistance(Data d){
        double newDistance = 0.0;
        // The datapool with particles that are not in same category with d
        DataPool dpDiff = new DataPool(dp);
        dpDiff.deleteData(this.getSameC(d.C));
        
        // Get min distance from d
        double minDistanceRaw = 0.0;
        for(int j=0;j<dpDiff.getData(0).getNumAttr();j++){
            minDistanceRaw += Math.abs(dpDiff.getData(0).attributes.get(j)-d.attributes.get(j));
        }
        int minIndex = 0;
        double newDistanceRaw = 0.0;
        for(int i=1;i<dpDiff.getNumData();i++){
            newDistanceRaw = 0.0;
            for(int j=0;j<dpDiff.getData(i).getNumAttr();j++){
                newDistanceRaw += Math.abs(dpDiff.getData(i).attributes.get(j)-d.attributes.get(j));
            }
            minDistanceRaw = getMin(minDistanceRaw, newDistanceRaw);
            minIndex = minDistanceRaw == newDistanceRaw ? i : minIndex;
        }
        for (int i = 0; i < dpDiff.getData(minIndex).getNumAttr(); i++) {
                newDistance += Math.pow(Math.abs(dpDiff.getData(minIndex).attributes.get(i) - d.attributes.get(i)),2);
        }
        double minDistance = Math.pow(newDistance, 0.5);
        return minDistance*0.5;
    }

    /**
     *
     * <summary>get a relative centroid of given data</summary>
     * <param name="tarCluster">targetCluster</param>
     * <return>a relative centroid</return>
     */
    public Centroid getCentroid(DataPool tarCluster) {
        DataPool dpTemp = new DataPool(tarCluster);
        while(dpTemp.getNumData()>1)
        {
            Random rnd = new Random();
            int first=(int) rnd.nextInt((dpTemp.getNumData()-1));
            int second=first+1;
            {
                dpTemp.addData(getCenterPoint(dpTemp.getData(first),dpTemp.getData(second)));
                dpTemp.deleteData(second);
                dpTemp.deleteData(first);
            }
        }
        Centroid c = new Centroid(dpTemp.getData(0));
        return c;
    }

    /**
     *
     * <summary>get a min number</summary>
     * <param name="firstNum">first number</param>
     * <param name="secondNum">second number</param>
     * <return>smaller one among two</return>
     */
    private double getMin(double firstNum, double secondNum) {
        return firstNum < secondNum ? firstNum : secondNum;
    }
    
    /**
     *
     * <summary>get a virtual pattern that sits in the middle of d1 and d2</summary>
     * <param name="d1">first pattern</param>
     * <param name="d2">second pattern</param>
     * <return>middle point</return>
     */
    private Data getCenterPoint(Data d1, Data d2)
    {
        Data d0 = new Data();
        for (int i = 0; i < d1.getNumAttr(); i++) {
            d0.attributes.add((d1.attributes.get(i) + d2.attributes.get(i)) / 2);
        }
        d0.C = d1.C;
        return d0;
    }    
}
