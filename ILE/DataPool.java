package ILE;

import java.util.Vector;
import java.io.*;
import java.util.StringTokenizer;

/**
 *
 * @author Jinghao Song
 */
public class DataPool {

    private Vector<Data> datas;
    private int numAttr;
    private int numClass;

    /**
     * <summary>Default Constructor</summary>
     */
    public DataPool() {
        datas = new Vector<Data>();
    }

    /**
     * <summary>
     * Constructor for Datapool with attribute number and class number
     * </summary>
     * <param name="_numAttr">number of attribute</param>
     * <param name="_numClass">the class it belongs to</param>
     */
    public DataPool(int _numAttr, int _numClass) {
        numAttr = _numAttr;
        numClass = _numClass;
        datas=new Vector<Data>();
    }

    /**
     *
     * <summary>Initialize with another DataPool</summary>
     * <param name="dp">DataPool for initializing</param>
     */
    public DataPool(DataPool dp) {
        //datas=dp.datas;
        numAttr = dp.numAttr;
        numClass = dp.numClass;
        datas = new Vector<Data>();
        for (int i = 0; i != dp.getNumData(); ++i) {
            Data d = new Data(dp.getData(i));
            datas.add(d);
        }
    }

    /**
     *
     * <summary>Constructor which loads the data from file</summary>
     * <param name="filename">the file name</param>
     */
    public DataPool(String filename) {
        loadFromFile(filename);
    }

    /**
     *
     * <summary>Return the size of the DataPool</summary>
     * <returns>size of data</returns>
     */
    public int getNumData() {
        return datas.size();
    }
    
    /**
     *
     * <summary>Return the number of classes</summary>
     * <returns>number of classes</returns>
     */
    public int getNumClass()
    {
        return numClass;
    }

    /**
     * <summary>Return the element in the position pos</summary>
     * <param name="pos">position of the element</param>
     * <returns>an element</returns>
     */
    public Data getData(int pos) {
        return datas.get(pos);
    }

    /**
     *
     * <summary>load the data fromm file</summary>
     * <param name="filename">the file name</param>
     */
    public void loadFromFile(String filename) {
        File file = new File(filename);
        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(file));
            numAttr = Integer.parseInt(br.readLine());
            numClass = Integer.parseInt(br.readLine());
            int numEntity = Integer.parseInt(br.readLine());

            datas = new Vector<Data>(numEntity);

            String line;
            Data tempData;
            Vector<Double> tempAttri;
            //Fill datas with Data object
            for (int i = 0; i != numEntity; i++) {
                tempAttri = new Vector<Double>(numAttr);
                //datas.get(i).attributes.setSize(numAttri);
                line = br.readLine();
                StringTokenizer st = new StringTokenizer(line);
                //Fill the attributes Vector with data
                for (int j = 0; j != numAttr; j++) {
                    double attri = Double.parseDouble(st.nextToken());
                    tempAttri.add(attri);
                }

                int classType = Integer.parseInt(st.nextToken());

                tempData = new Data();
                tempData.setAttr(tempAttri);
                tempData.setClass(classType);
                datas.add(tempData);
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * <summary>
     * Unlike the getData() function, this function will delete the data after
     * retrieving it.
     * </summary>
     * <param name="pos">position</param>
     * <returns>Data</returns>
     */
    public Data retrieveData(int pos) {
        Data dataCache = datas.get(pos);
        deleteData(pos);
        return dataCache;
    }
    
    /**
     * 
     * <summary>add a data d</summary>
     * <param name="d">data</param>
     */
    public void addData(Data d)
    {
//        for (int i = 0; i < this.datas.size(); i++)
//        {
//            if (datas.get(i).attributes.equals(d.attributes)
//                    && datas.get(i).C == d.C)   return;
//        }
        datas.add(d);
    }
    
    /**
     * 
     * <summary>Add a DataPool structure to a DataPool</summary>
     * <param name="dp">DataPool</param>
     */
    public void addData(DataPool dp)
    {
        for(int i=0;i<dp.getNumData();i++)
        {
            this.addData(dp.getData(i));
        }
    }

    /**
     *
     * <summary>delete the data in the specific position</summary>
     * <param name="pos">position</param>
     */
    public void deleteData(int pos) {
        datas.remove(pos);
    }
    
    /**
     * 
     * <summary>delete specific data</summary>
     * <param name="data">data</param>
     * <return>
     * bool value indicates the success or failure of the operation
     * </return>
     */
    public boolean deleteData(Data data)
    {
        Boolean delFlag = false;
        for(int i=0;i<this.datas.size();i++)
        {
            if(datas.get(i).attributes.equals(data.attributes) && datas.get(i).C == data.C)
            {
                // call the original delete function and leave everything to the lib
                this.deleteData(i); 
                delFlag = true;
            }
        }
        return delFlag;
    }
    
    /**
     * 
     * <summary>delete all data in a dp from current DataPool</summary>
     * <param name="dp">DataPool</param>
     */
    public void deleteData(DataPool dp)
    {
        for(int i=0;i<dp.datas.size();i++)
        {
            // Call the single deletion function
            this.deleteData(dp.getData(i));
        }
    }
    
    public void printOutDataPool()
    {
        int size = this.datas.size();
        System.out.println("Size: "+size);
        for(int i=0;i<size;i++)
        {
            System.out.println(this.datas.get(i).attributes.toString());
                    //+"\tClass: "
                    //+this.datas.get(i).C);
        }
    }
    
    public void writeToFile(String filename) {
        File file = new File(filename);
        FileWriter fw;
        try {
            fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            int numEnt = this.getNumData();

            System.out.printf("numEnt: %d\n", numEnt);

            bw.write(String.valueOf(numAttr)+"\r\n");
            bw.write(String.valueOf(numClass)+"\r\n");
            bw.write(String.valueOf(numEnt)+"\r\n");

            for (int i = 0; i != numEnt; i++) {
                for (int j = 0; j != numAttr; j++) {
                    double attri = datas.get(i).attributes.get(j);
                    bw.write(attri + "\t");
                }
                int c = datas.get(i).C;
                bw.write(String.valueOf(c));
                bw.newLine();
            }
            bw.close();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public int getNumAttr()
    {
        numAttr=datas.get(0).getNumAttr();
        return numAttr;
    }
    
    public double findLeftBound(int dim)
    {
        double bound=datas.get(0).get(dim);

        for(int i=1;i!=datas.size();i++)
        {
            if(datas.get(i).get(dim)<bound)
                bound=datas.get(i).get(dim);
        }
        return bound;
    }

    public double findRightBound(int dim)
    {
        double bound=datas.get(0).get(dim);

        for(int i=1;i!=datas.size();i++)
        {
            if(datas.get(i).get(dim)>bound)
                bound=datas.get(i).get(dim);
        }
        return bound;
    }
}
