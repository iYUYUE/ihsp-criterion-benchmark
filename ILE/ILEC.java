
package ILE;

import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class ILEC {
    DataPool trainDP;
    DataPool valiDP;
    Vector<ExpertGroup> expGs;
    public static GUIClass gui;
    boolean enableGUI = false;
    double extendRate;
    int level;
     /**
      * 
      * <summary>
      * Constructor. Requiring data for training solution
      * </summary>
      */
    public ILEC(DataPool trainData, DataPool valiData, boolean enableGui, double ext, int lv){
        trainDP=trainData;
        valiDP=valiData;
        this.enableGUI = enableGui;
        this.extendRate = ext;
        this.level = lv;
    }
    
    public void train(double goal)
    {
        int numClass=trainDP.getNumClass();
        expGs=new Vector<ExpertGroup>(numClass);
        if(this.enableGUI){
            gui = new GUIClass(trainDP);
        }
        //Used for storing the accuracy of each class expert group
        Vector<Double> fitnessTable=new Vector(numClass);
        
        /**Train the solution for each of the Class. 
           Trainer class is called to train the solu. 
           */
        for(int i=0;i!=numClass;i++)
        {
            if(level>0)
                System.out.println("Training Experts Group: Class "+(i+1));
            Trainer tr;
            if(extendRate>0)
                tr=new Trainer(trainDP,valiDP, gui, enableGUI, extendRate, level);
            else
                tr=new Trainer(trainDP,valiDP, gui, enableGUI, level);
            expGs.add(tr.train(goal,i+1));  //train expG for class i+1 until reach the goal
            fitnessTable.add(testExpert(expGs.get(i))); // evaluate the fitness of the newly trained solu
        }
        
        /**Sort the ExpertGroups and FitnessTable. Bubble Sort is utilized.
         */
        for(int i=0;i!=numClass;i++)
        {
            double max=fitnessTable.get(i);
            int maxIndex=i;
            for(int j=i;j!=numClass;j++)
            {
                if(fitnessTable.get(j)>max){
                    max=fitnessTable.get(j);
                    maxIndex=j;
                }
            }
            double tmpFit=max;
            ExpertGroup tmpExp=expGs.get(maxIndex);
            
            expGs.setElementAt(expGs.get(i), maxIndex);
            expGs.setElementAt(tmpExp, i);
            fitnessTable.setElementAt(fitnessTable.get(i),maxIndex);
            fitnessTable.setElementAt(tmpFit, i);
        }
    }
    
    /**
     * Test the fitness of a given expG.
     * ExpertGroup.judge() method will be called to judge the type of data.
     * when judging data from the trainDP. expG will get score if meeting either of the following conditions:
     * 1. expG accept the data & that data is of the same class as the expG.
     * 2. expG reject the data & that data is not of the same class as the expG.
     */
    public double testExpert(ExpertGroup expG)
    {
        DataPool tdp = trainDP;
        int numData = tdp.getNumData();
        int score = 0;
        for(int i=0;i!=numData;i++)
        {
            Data d = tdp.getData(i);
            boolean res = expG.judge(d);
            if(res && d.getDClass()==expG.experts.get(0).C)
                ++score;
            else if(!res && expG.experts.isEmpty())
                ;
            else if(!res && d.getDClass()!=expG.experts.get(0).C)
                ++score;
        }

        return 1.0*score/numData;
    }
    
    //Use the testData to calculate the fitness of the trained expGs--The whole solution.
    /**
     * Test the finess of expGs, the complete solution.
     * The expGs will get score if:
     * 1. there is one expG in the expGs that can jugde the given data.
     * & 2. the class of the responsible expG match the judging data.
     * 
     * ExpertGroup.judge() method will be called to judege data class
     */
    public double test(DataPool testData)
    {
        int numData = testData.getNumData();
        int score = 0;
        for(int i=0;i!=numData;i++)
        {
            int result = 1;     // initially the data is assumed to be of class 1, as expGs can at least judge one class.
            Data d = testData.getData(i);
            int numExpertG = expGs.size();

            /**For each of the expG in expGs, check if it can judge the data*/
            for(int j=0;j!=numExpertG;j++)
            {
                if(expGs.get(j).judge(d) == true){
                    result = expGs.get(j).experts.get(0).C;//All the experts within one expG will determine the same class, so it is ok to call the first
                    break;
                }
            }
            /**Check the judge result*/
            if(result == d.getDClass())
                score++;
        }
        return 1.0*score/numData;
    }
}
