package ILE;

import java.util.Random;
import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class EEEncoder {
    int numRule;
    int numAttr;
    int C;
    double leftBound;
    double rightBound;
    Centroid cent = new Centroid();;
    DataPool dp = new DataPool();
    Random random = new Random();
    static final double ZERO_PROB = 0.8;
    static final double RADIUS_ZERO_PROB = 1; // currently inner radius will definitely be zero.
    
    public EEEncoder(int _numRule, int _numAttr, int _C,
            double _leftBound, double _rightBound) {
        numRule = _numRule;
        numAttr = _numAttr;
        C = _C;
        leftBound = _leftBound;
        rightBound = _rightBound;
    }
    
    public EEEncoder(int _numRule, int _numAttr, int _C,
            Centroid _cent, DataPool _dp,
            double _leftBound, double _rightBound)
    {
        this.numRule = _numRule;
        this.numAttr = _numAttr;
        this.C = _C;
        this.cent = _cent;
        this.dp = _dp;
        this.leftBound = _leftBound;
        this.rightBound = _rightBound;
    }
    
    public Vector<Double> randomize()
    {
        //the parameter code can not be modified directly, so we create a temp variable and return it at last
        Vector<Double> temp_code;

        int ruleLen = numAttr + 2; //At the end of each rule, one number for OUTER radius, one number for INNER radius
        //!!!(first number is not the class type, since one rule can only decide which side will be chosen. It is the expert that will use rules to decide a class)
        int chromLen = ruleLen * numRule + 1; //The first position in the chrom is for class type
        temp_code=new Vector<Double>(chromLen); //One chrom represents a single expert

        temp_code.add((double)C);//Set the detecting Class Type (the first bit in codes)

        for(int i=0;i!=numRule;i++)
        {
            int base=ruleLen*i+1;
            
            double val = 0.0;

            //Set the coefficients which represents the centroid
            for(int j=0;j!=ruleLen-2;j++)
            {
                //0 will have a higher probability to be generated.
                double isZero=random.nextDouble();

                if(isZero<ZERO_PROB)
                    temp_code.add(0.0);
                else
                {
                    val=2.0*random.nextDouble()-1;
                    temp_code.add(val);
                    if(temp_code.get(base+j)==0.0)
                        temp_code.set(base+j,0.0001);
                }
            }

            //set the constant (last position within one rule)
//            val=leftBound+random.nextDouble()*(rightBound-leftBound);
//            temp_code.add(val);
            double innerRadius = 0.0;;
            double isZero=random.nextDouble();
            if(isZero>RADIUS_ZERO_PROB) innerRadius = random.nextDouble(); // innerRadius has higher probability to be 0.
            double outerRadius = innerRadius + random.nextDouble() * (rightBound - leftBound)/2;
            temp_code.add(outerRadius);
            temp_code.add(innerRadius);
            
//            val += random.nextDouble();
//            temp_code.add(val);

        }
        return temp_code;
    }

    public Vector<Double> initialize()
    {
        Vector<Double> temp_code;
        int ruleLen = numAttr + 2; //At the end of each rule, one number for OUTER radius, one number for INNER radius
        //!!!(first number is not the class type, since one rule can only decide which side will be chosen. It is the expert that will use rules to decide a class)
        int chromLen = ruleLen * numRule + 1; //The first position in the chrom is for class type
        temp_code=new Vector<Double>(chromLen); //One chrom represents a single expert
        
        temp_code.add((double)C);//Set the detecting Class Type (the first bit in codes)
        
        for (int i = 0; i != numRule; i++) {
            int base = ruleLen * i + 1;
            double val = 0.0;

            //Set the coefficients which represents the centroid
            for (int j = 0; j != ruleLen - 2; j++) {
                temp_code.add(this.cent.attributes.get(j)+((2*random.nextDouble()-1) * (rightBound - leftBound)/2)); //changed
            }
            double innerRadius = 0.0;;
            double isZero=random.nextDouble();
            if(isZero>RADIUS_ZERO_PROB) innerRadius = random.nextDouble(); // innerRadius has higher probability to be 0.
            double outerRadius = innerRadius + random.nextDouble() * (rightBound - leftBound)/2;
            temp_code.add(outerRadius);
            temp_code.add(innerRadius);
        }
        return temp_code;
    }
    
    public void mutate(Vector<Double> code, double mutateRate)
    {
        int ruleLen=numAttr+2;
        int chromLen=ruleLen*numRule+1;
        double randVal;
        double val;

        for(int i=0;i!=numRule;i++)
        {
            int base=i*ruleLen+1;
            //To mutate coefficients
            for(int j=0;j!=ruleLen-2;j++)
            {
                randVal=random.nextDouble();
                if(randVal<mutateRate)
                {
                    //0 will have a higher probability to be generated
                    double isZero=random.nextDouble();

                    if(isZero<ZERO_PROB)
                        code.setElementAt(0.0, base+j);
                    else
                    {
                        val=2.0*random.nextDouble()-1.0;
                        code.set(base+j, val);
                        if(code.get(base+j)==0)
                            code.set(base+j, 0.0001);
                    }
                }
            }

            //To mutate the radius
            randVal=random.nextDouble();
            if(randVal<mutateRate)
            {
                val=randVal+random.nextDouble()*(rightBound-leftBound);
                code.set(base+ruleLen-2, val); // outer
                code.set(base+ruleLen-1, randVal); // inner
            }
        }
    }
    
}
