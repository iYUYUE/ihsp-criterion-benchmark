/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ILE;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author LEO
 */
public class GUIClass extends JFrame {

    public DataPool datapool;
    public Centroid cent = new Centroid();
    public double innerR = 0;
    public double outerR = 0;
    private int extend = 5;

    GUIClass(DataPool dp) {

        super("Visualization");
        
        datapool = dp;
        //buildGUI();

        cent.attributes.add(0.0);
        cent.attributes.add(0.0);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        this.setSize(600, 600);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        for (int i = 0; i < datapool.getNumData(); i++) {
            Data data = datapool.getData(i);
            int x = (int)(data.attributes.get(0).doubleValue()*extend);
            int y = (int)(data.attributes.get(1).doubleValue()*extend);
            if (data.C == 1) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.BLUE);
            }
            g.fillOval(x, y, 5, 5);
        }
        g.setColor(Color.black);
//        g.fillOval(5*89,5*11,10,10);
        g.fillOval(5* cent.attributes.get(0).intValue(), 5*cent.attributes.get(1).intValue(), 6, 6);
        g.drawOval((int)(extend* cent.attributes.get(0)-extend*innerR), (int)(extend*cent.attributes.get(1)-extend*innerR), (int)(innerR*extend*2), (int)(innerR*extend*2));
        g.drawOval((int)(extend* cent.attributes.get(0)-extend*outerR), (int)(extend*cent.attributes.get(1)-extend*outerR), (int)(outerR*extend*2), (int)(outerR*extend*2));
    }

    
}
