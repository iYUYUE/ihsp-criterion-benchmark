package ILE;

import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class Centroid {

    Vector<Double> attributes;

    public Centroid() {
        this.attributes = new Vector<Double>();
    }

    public Centroid(Data d) {
        this.attributes = d.attributes;
    }
    
    public Centroid(Centroid c)
    {
        this.attributes = c.attributes;
    }
    
    public Centroid(Vector<Double> v)
    {
        this.attributes = v;
    }
    
    public void PrintCentroid()
    {
        System.out.print("[");
        for(int i=0;i<this.attributes.size();i++)
        {
            System.out.print(this.attributes.get(i)+"\t");
        }
        System.out.print("]");
    }
}
