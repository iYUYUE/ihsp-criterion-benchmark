package ILE;

import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class GASolver {
    double crossoverRate;
    double mutateRate;
    Vector<Chromosome> matingPool;  // better chroms selected from the current generation for producting next generration
    Vector<Chromosome> chroms;
    Vector<Double> fitnessTable;    // fitness of each of the chrom, used for selection
    Vector<Integer> correctNumTable; // number of patterns well classified
    EEDecoder decoder;
    EEEncoder encoder;
    int bestChromIndex;
    double bestFitness;
    double averageFitness;
    
     public GASolver(double _crossoverRate, double _mutateRate, int population, int matingPoolSize, int crossoverType,
            EEDecoder dec, EEEncoder enc)
    {
        crossoverRate=_crossoverRate;
        mutateRate=_mutateRate;
        chroms=new Vector<Chromosome>(population);
        matingPool=new Vector<Chromosome>(matingPoolSize);
        fitnessTable=new Vector<Double>(population);
        correctNumTable = new Vector<Integer>(population);
        encoder=enc;
        decoder=dec;
        
        //add new chromosome (Encoder is encoder) into the chroms
        for(int i=0;i!=population;i++)
        {
            chroms.add(new Chromosome(encoder));
            fitnessTable.add(0.0);
            correctNumTable.add(0);
        }
    }
     
     public boolean train(int generation, double goal)
    {
        int count=0;
        evaluate();
        findStatistic();
        while(bestFitness<goal && count<generation)
        {
            double oldBest=bestFitness;
            evaluate();
            select();
            crossover();
            mutate();
            sort();
            replace();
            findStatistic();
            clear();
            if(bestFitness<=oldBest)//termination condition
                ++count;
            else
                count=0;
        }
        if(bestFitness<goal)
            return false;
        return true;
    }
     
     public void evaluate()
    {
        int population=chroms.size();
        for(int i=0;i!=population;i++)
        {
            double fitness=decoder.fitness(chroms.get(i));
            fitnessTable.setElementAt(fitness,i);
            int correctNum = decoder.getCorrectNum();
            correctNumTable.setElementAt(correctNum, i);
        }
    }
     
     //Select the chroms with high fitness from current population and copy it to the matingPool.
    //The probability to be selected increases proportionly according to the fitness of the chrom.
    public void select()
    {
        Scanner scan=new Scanner(System.in);
        Random random=new Random();

        int population=fitnessTable.size();
        Vector<Double> probTable=new Vector(population); //Used collitively with fitness table for selecting the chroms with high fitness
        double probSum=0.0;

        for(int i=0;i!=population;i++)
        {
            probSum+=fitnessTable.get(i)+0.0001;//??????????
        }
        if(probSum<=0.001)
        {
            System.out.println("Warning: The Fitness Sum is Too Low!\n\n");
            String pause=scan.next();//Pause the program for displaying the message
        }

        for(int i=0;i!=population;i++)
        {
            double value=(fitnessTable.get(i)+0.0001)/probSum;
            probTable.add(value);
        }

        int mps=matingPool.size();
        for(int i=0;i!=mps;i++)
        {
            double selector=random.nextDouble();
            if(selector==1.0)
                selector-=0.00000001;
            double currentProb=probTable.get(0);
            int index=0;
            while(currentProb<selector)
            {
                ++index;
                currentProb+=probTable.get(index);
            }

            
            if(index>=probTable.size())
                --index;
            
            Chromosome chosen=new Chromosome(encoder);
            chosen=chroms.get(index);
            matingPool.add(chosen);
        }
    }
    
    public void findStatistic()
    {
        double fitnessSum=0.0;
        bestChromIndex=0;
        bestFitness=fitnessTable.get(0);
        int population=fitnessTable.size();

        // Find best fitness
        for(int i=0;i!=population;i++)
        {
            if(fitnessTable.get(i)>bestFitness)
            {
                bestFitness=fitnessTable.get(i);
                bestChromIndex=i;
            }
            fitnessSum+=fitnessTable.get(i);
        }
        // Find chromosome with best fitness and most well classified patterns
        for(int i=0;i!=population;i++){
            if(fitnessTable.get(i)>=bestFitness && correctNumTable.get(i)>=correctNumTable.get(bestChromIndex))
            {
                bestChromIndex=i;
            }
        }
        averageFitness=fitnessSum/population;
    }
    
    public void crossover()
    {
        Chromosome chrom=new Chromosome(encoder);
        int mpsHalf=matingPool.size()/2;
        for(int i=0;i!=mpsHalf;i++)
        {
            int base=i*2;
            //matingPool.get(base).crossover(matingPool.get(base),matingPool.get(base+1),crossoverRate);
            chrom.crossover(matingPool.get(base),matingPool.get(base+1),crossoverRate);//???????????
        }
    }
    
    public void mutate()
    {
        int mps=matingPool.size();
        for(int i=0;i!=mps;i++)
        {
            matingPool.get(i).mutate(mutateRate);
        }
    }
    
    //Sort chroms and fitnessTable into fitness increasing order
    public void sort()
    {
        int population=chroms.size();
        for(int i=0;i!=population;i++)
        {
            int minIndex=i;
            double minVal=fitnessTable.get(i);
            for(int j=i;j!=population;j++)
            {
                if(fitnessTable.get(j)<minVal)
                {
                    minIndex=j;
                    minVal=fitnessTable.get(j);
                }
            }

            // Update chromosomes
            Chromosome tempChrom=chroms.get(minIndex);
            chroms.setElementAt(chroms.get(i), minIndex);
            chroms.setElementAt(tempChrom,i);
            
            // Update well classified table
            int tempCorrectNum = correctNumTable.get(minIndex);
            correctNumTable.setElementAt(correctNumTable.get(i), minIndex);
            correctNumTable.setElementAt(tempCorrectNum, i);
            
            // Update fitness table
            double tmpFit=fitnessTable.get(minIndex);
            fitnessTable.setElementAt(fitnessTable.get(i),minIndex);
            fitnessTable.setElementAt(tmpFit, i);
        }
    }
    
    //update chroms with the matingPool.
    public void replace()
    {
        int mps=matingPool.size();
        for(int i=0;i!=mps;i++)
        {
            chroms.setElementAt(matingPool.get(i), i);
            fitnessTable.setElementAt(decoder.fitness(chroms.get(i)), i);
        }
    }
    
    //clear up the matingPool for futher usage.
    public void clear()
    {
        matingPool.clear();
    }
}
