package ILE;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

import java.util.concurrent.ExecutorService; 
import java.util.concurrent.Executors;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;

/**
 * @version IHSP Criterion Benchmark Demo (based on RM_Update1 by Jinghao Song)
 * @author Yue Yu
 */

public class Main {
    // the pool of worker threads
    private static ExecutorService workers;
    
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        // times to run 
        int times = 8;
        // multithread option (disabled by default)
        int threadNum = 1;
        // outport level
        int level = 2;
        // use the default extension rate (ext<=0) or assign another one
        double[] extArray = null;
        // tasks
        List<String> trainningTasks = new LinkedList<String>();

        int numOfTasks;

        for(int i=0; i<args.length; i++) {
            if(!args[i].startsWith("-")) {
                trainningTasks.add(args[i]);
            }
            
        }

        numOfTasks = trainningTasks.size();

        if(numOfTasks>0){

        } else {
            System.err.println("Input Format: task1 task2 task3 ... [options]");
            System.exit(-1);
        }

        for(int i=0; i<args.length; i++) {
            if(args[i].startsWith("-t")) {
                try
                {
                   times = Integer.parseInt(args[i].substring(2, args[i].length()));
                }
                catch (NumberFormatException nfe)
                {
                    System.err.println("Input parameter error (-t)");
                    System.exit(-1);
                }

            }
            else if(args[i].startsWith("-n")) {
                try
                {
                   threadNum = Integer.parseInt(args[i].substring(2, args[i].length()));
                }
                catch (NumberFormatException nfe)
                {
                    System.err.println("Input parameter error (-t)");
                    System.exit(-1);
                }

            }
            else if(args[i].startsWith("-l")) {
                try
                {
                   level = Integer.parseInt(args[i].substring(2, args[i].length()));
                }
                catch (NumberFormatException nfe)
                {
                    System.err.println("Input parameter error (-l)");
                    System.exit(-1);
                }

            }
            // else if(args[i].startsWith("-e")) {
            //     try
            //     {
            //        ext = Double.parseDouble(args[i].substring(2, args[i].length()));
            //     }
            //     catch (NumberFormatException nfe)
            //     {
            //         System.err.println("Input parameter error (-e)");
            //         System.exit(-1);
            //     }
            // }
            else if(args[i].startsWith("-e")) {
                try
                {
                    String tmp = args[i].substring(2, args[i].length());
                    String[] tmpArray = tmp.split("\\/");
                    System.err.println(tmpArray.length);
                    if(tmpArray.length==numOfTasks||tmpArray.length==1){
                        extArray = new double[tmpArray.length];
                        for(int j=0; j<tmpArray.length; j++)
                            extArray[j] = Double.parseDouble(tmpArray[j]);
                    } else {
                        System.err.println("Input parameter error (-e number)");
                        System.exit(-1);
                    }
                }
                catch (NumberFormatException nfe)
                {
                    System.err.println("Input parameter error (-e format)");
                    System.exit(-1);
                }
            }
        }

        workers = Executors.newFixedThreadPool(threadNum);

        System.out.println("Running Times: "+times+"  Extension Rate: "+((extArray!=null)?Arrays.toString(extArray):"DEFAULT")+"  Thread Number: "+threadNum);

        System.out.print("\nPress Enter to continue ...");
        scanner.nextLine();

        for (int i=0; i<numOfTasks; i++) {
            // String datasetName = "hill";
            // resultLog = "";
            // String datasetName =  args[i];
            // resultLog += datasetName + "\n";
            // trainWithValidation(datasetName, times, false, ext);
            // writeLog(datasetName);
            workers.submit(
                new ThreadHandler(trainningTasks.get(i), times, (extArray!=null&&extArray.length>0)?(extArray.length==1?extArray[0]:extArray[i]):-1.1, false, level));
            
        }

    }

    private static String train(String datasetName, int times, boolean enableGUI, double ext, int level) {
        String resultLog = "";
        for (int i = 0; i < times; i++) {
            System.out.println("#######\nTrial "+i+"\n#######\n");
            resultLog += "Trial "+i+"\n#######\n";
            DataPool dp = new DataPool("./datasets/" + datasetName + "_train.txt");          // dataset for training solution
            DataPool vdp = new DataPool();
            DataPool tdp = new DataPool("./datasets/" + datasetName + "_test.txt");    // dataset for testing the accuracy of the solution
            resultLog += trainAndTest(dp, vdp, tdp, enableGUI, ext, level);
        }
        return resultLog;
    }

    private static String trainWithValidation(String datasetName, int times, boolean enableGUI, double ext, int level) {
        String resultLog = "";
        for (int i = 0; i < times; i++) {
            System.out.println("##########\n"+datasetName+" Trial "+i+"\n##########\n");
            resultLog += "Trial "+i+"\r\n#######\r\n";
            DataPool dp = new DataPool("./datasets/" + datasetName + "_v_train.txt");          // dataset for training solution
            DataPool vdp = new DataPool("./datasets/" + datasetName + "_v_vali.txt");
            DataPool tdp = new DataPool("./datasets/" + datasetName + "_v_test.txt");    // dataset for testing the accuracy of the solution
            resultLog += trainAndTest(dp, vdp, tdp, enableGUI, ext, level);
        }
        return resultLog;
    }
    
    private static String trainAndTest(DataPool dp, DataPool vdp, DataPool tdp, boolean enableGUI, double ext, int level){
            /**
             * Training & evaluating
             */
            ILEC ilec = new ILEC(dp, vdp, enableGUI, ext, level);
            // Training: record the training time
            long startTime = System.currentTimeMillis();
            ilec.train(0.99);
            long endTime = System.currentTimeMillis();

            double time = (endTime - startTime) / 1000;
            // Evaluating

            double trainFitness = ilec.test(dp);
            double testFitness = ilec.test(tdp);

            /**
             * Output the result and wait of an input before terminating.
             */
            // System.out.println(startTime);
            // System.out.println(endTime);
            System.out.printf("The trainging time is(in second):%f\n\n", time);
            System.out.printf("The train fitness is: %f\n\n", trainFitness);
            System.out.printf("The test fitness is: %f\n\n", testFitness);

            // clean up cache memory
            ilec=null;
            System.gc();
            
            return startTime + "\t" + endTime + "\t" + trainFitness + "\t" + testFitness + "\t" + time + "\r\n";
    }
    
    public static void writeLog(String filename, String resultLog) {
        File file = new File("./Log/" + filename + ".txt");
        FileWriter fw;
        try {
            fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(resultLog);
            bw.close();
            fw.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class ThreadHandler implements Runnable
    {
        String datasetName;
        int times;
        int level;
        double ext;
        boolean gui;

        ThreadHandler(String name, int t, double e, boolean g, int l)
        {
            datasetName = name;
            times = t;
            ext = e;
            gui = g;
            level = l;
        }

        public void run()
        {
            String resultLog = "";
            resultLog += datasetName + "\n";
            resultLog += trainWithValidation(datasetName, times, gui, ext, level);
            writeLog(datasetName, resultLog);
        }

    }
}
