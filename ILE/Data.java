package ILE;

import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class Data {

    Vector<Double> attributes;
    int C = -1;

    /**
     *
     * <summary>Default Constructor</summary>
     */
    public Data() 
    {
        attributes = new Vector<Double>();
    }

    /**
     * <summary>Constructor initiated with Data d</summary>
     * <param name="d">Data element</param>
     */
    public Data(Data d) 
    {
        C = d.C;
        attributes = new Vector<Double>();
        for (int i = 0; i != d.attributes.size(); ++i) {
            attributes.add(d.attributes.get(i));
        }
    }

    /**
     *
     * <summary>Constructor with capacity specified</summary>
     * <param name="dataNum">initialCapacity</param>
     * <param name="increment">capacityIncrement</param>
     */
    public Data(int dataNum, int increment) 
    {
        attributes = new Vector<Double>(dataNum, increment);
    }
    
    /**
     *
     * <summary>get the class number</summary>
     * <return>indicates the class number</return>
     */
    public int getDClass()
    {
        return C;
    }
    
    /**
     * <summary>get the ADT of attributes</summary>
     * <return>ADT of attributes</return>
     */
    public Vector<Double> getAttr()
    {
        return attributes;
    }
    
    /**
     * 
     * <summary>get the size of the attribute</summary>
     * <return>size of the attributes</return>
     */
    public int getNumAttr()
    {
        return attributes.size();
    }

    /**
     *
     * <summary>set the attribute</summary>
     * <param name="attr">attribute</param>
     */
    public void setAttr(Vector<Double> attr) 
    {
        attributes = attr;
    }

    /**
     * <summary>set the class for the pattern</summary>
     * <param name="c">class number</param>
     */
    public void setClass(int c) 
    {
        C = c;
    }
    
    public double get(int pos)
    {
        return attributes.get(pos);
    }
    
    public void add(double val)
    {
        attributes.add(val);
    }
}
