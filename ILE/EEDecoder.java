package ILE;

/**
 *
 * @author Jinghao Song
 */
public class EEDecoder {
    DataPool dp;
    int numRule;
    Expert exp;
    private int correct = 0;
    
    public EEDecoder(DataPool _dp, int _numRule)
    {
        dp=_dp;
        numRule=_numRule;
        exp=initializeExpert();
    }
    
    public Expert initializeExpert()
    {
        deleteExpert();
        exp=new Expert();
        return exp;
    }
    
    public void deleteExpert()
    {
        if(exp!=null)
            exp=null;
    }
    
    //Calculate the fitness of the entered chromosome
    public double fitness(Chromosome chrom) {
        int numData = dp.getNumData();
        correct = 0;
        int answer = 0;

        for (int i = 0; i != numData; i++) {
            int C = classify(i, chrom);
            Data data = dp.getData(i);
            if (C != -1 && C == data.getDClass()) {
                correct++;
                answer++;
            } else if (C != -1 && C != data.getDClass()) {
                answer++;
            } else if (C == -1 && data.getDClass() == exp.C && exp.judge(data)) {
                //answer++; // What's this expert? This seems to be trivial and useless
            }
        }
        if (answer != 0) {
            //System.out.println("correct = "+correct+"\nanswer = "+answer);
            return 1.0 * correct / answer;
        }

        return 0;
    }
    
    public int getCorrectNum(){
        return this.correct;
    }
    
    public int classify(int index, Chromosome chrom) {
        Data data = dp.getData(index);
        boolean classifyFlag = true;
        int numAttr = dp.getNumAttr();
        int ruleLen = numAttr + 2;

        // Ensure that only data within the given Exp will be check by the Chrom
        if (exp.judge(data) == false) {
            return -1;
        }

        // (X1-C1)^2 + (X2-C2)^2 + ... + (Xn-Cn)^2 =?= R^2
        for (int i = 0; i != numRule; i++) {
            double val = 0.0;
            int base = ruleLen * i + 1;
            for (int j = 0; j != numAttr; j++) {
                val += Math.pow(data.get(j) - chrom.code.get(base + j),2);
            }
            double val1 = val - Math.pow(chrom.code.get(base + numAttr),2);
            double val2 = val - Math.pow(chrom.code.get(base+numAttr+1),2);

            if (val1>0 || val2 < 0) // out of range then false
            {
                classifyFlag = false;
                break;
            }
        }
        if (classifyFlag == true) {
            return chrom.code.get(0).intValue();//return the Class Type of this chromosome
        }
        return -1; // report error
    }
}
