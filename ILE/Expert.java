package ILE;
import java.util.Vector;
/**
 *
 * @author Jinghao Song
 * @author Steven Yu
 * 1. One expert can only accept or reject the given data. A group of experts can then determine
 * collectively whether the data actually belongs to the class determined by the expG.
 * 2. One class of data may distributed in various area, and is detected by espGps.
 *    The expert is responsible for one area of data detection.
 */
public class Expert {
    //Expert can only accept or reject the given data
    public static final boolean ACCEPT=true;
    public static final boolean REJECT=false;

    int C;//Record the class that this expert is responsible to determine
    Vector<Rule> rules;//All the rulse used in this expert

    public Expert()
    {
        C=-1;
        rules=new Vector<Rule>();//??The capacity of the initial rules is the default 10.
    }
    
    public Expert(int C, Rule rules){
        this.C = C;
        this.rules = new Vector<Rule>();
        this.rules.add(rules);
    }
    
    //Determine if the data can be accepted by this expert
    public boolean judge(Data d)
    {
        int numRules=rules.size();
        //If one rule reject the data, then the expert should reject the data
        for(int i=0;i!=numRules;i++)
        {
            if(!rules.get(i).isC(d))
                return REJECT;
        }

        return ACCEPT;
    }
    
    public void addRule(Rule r)
    {
        Rule rule=new Rule();
        rule=r;
        rules.add(rule);
    }
}
