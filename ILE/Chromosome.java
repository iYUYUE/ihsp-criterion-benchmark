package ILE;

import java.util.Random;
import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class Chromosome {
    Vector<Double> code;
    CrossoverType crossoverType=CrossoverType.UNIFORM;
    EEEncoder encoder;
    Random random=new Random();
    
    public enum CrossoverType
    {
        SINGLE_POINT,
        SEGMENTATION,
        UNIFORM
    }
    
    public Chromosome(EEEncoder enc)
    {
        encoder=enc;
        //randomlize();
        initialize();
    }
    
    public void randomize()
    {
        code=encoder.randomize();
    }
    
    public void initialize()
    {
        code = encoder.initialize();
    }
    
    public void crossover(Chromosome p1, Chromosome p2, double crossoverRate)
    {
        switch(crossoverType)
        {
            case SINGLE_POINT:
                coSinglePoint(p1,p2,crossoverRate);
                break;
            case SEGMENTATION:
                coSegmentation(p1,p2,crossoverRate);
                break;
            case UNIFORM:
                coUniform(p1,p2,crossoverRate);
                break;
        }
    }
    
    public void coSegmentation(Chromosome p1, Chromosome p2, double crossoverRate)
    {
        double rate=random.nextDouble();
        if(rate>crossoverRate)
            return;

        int pp1=0, pp2=0;
        int flip=0;
        int codeLen=p1.length();

        for(int i=0;i!=codeLen;i++)
        {
            double randVal=random.nextDouble();
            if(randVal>0.5)
            {
                if(flip==1)
                {
                    pp2=i;
                    for(int j=pp1; j<=pp2;j++)
                    {
                        double temp=p2.code.get(j);
                        p2.code.set(j, p1.code.get(j));
                        p1.code.set(j, temp);
                    }
                }
                else
                {
                    pp1=i;
                }
                flip=1-flip;
            }
        }
    }

    //choose a single point in in one chromosome. Exchange the two parts between the two chromosomes, seperated by the point
    public void coSinglePoint(Chromosome p1, Chromosome p2, double crossoverRate)
    {
        double rate=random.nextDouble();

        if(rate>crossoverRate)
            return;
        int codeLen=p1.code.size();
        int coPoint=random.nextInt(codeLen);
        if(coPoint==codeLen)
            coPoint--;

        for(int i=coPoint;i!=codeLen;i++)
        {
            double temp=p2.code.get(i);
            p2.code.set(i, p1.code.get(i));
            p1.code.set(i, temp);
        }
    }

    public void coUniform(Chromosome p1, Chromosome p2, double crossoverRate)
    {
        int codeLen=p1.code.size();
        for(int i=0;i!=codeLen;i++)
        {
            double randomRate=random.nextDouble();
            if(randomRate<crossoverRate)
            {
                double temp=p2.code.get(i);
                p2.code.set(i, p1.code.get(i));
                p1.code.set(i, temp);
            }
        }
    }
    
    public int length()
    {
        return code.size();
    }
    
    public void mutate(double mutateRate)
    {
        encoder.mutate(code, mutateRate);
    }
}
