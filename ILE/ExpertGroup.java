/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ILE;

import java.util.Vector;

/**
 *
 * @author Jinghao Song
 */
public class ExpertGroup {
    Vector<Expert> experts;

    public ExpertGroup()
    {
        experts=new Vector<Expert>();
    }
    
    //Judge if the data can be determined by the experts within this expG
    public boolean judge(Data d)
    {
        int numExps=experts.size();

        if(numExps==0)
        {
            //return true;
            return false;
        }

        //For each of the expert, if one of them can determine the data, the data should then be within the class
        for(int i=0;i!=numExps;i++)
        {
            if(experts.get(i).judge(d))
                return true;
        }

        return false;
    }
    
    public void addExpert(Expert exp)
    {
        experts.add(exp);
    }
}
